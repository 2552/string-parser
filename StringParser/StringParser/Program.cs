﻿using System;

namespace StringParser
{
    class Program
    {
        public static void Main (string[] args)
        {
            string text = "3[sd]";            
            StringTransformer stringTransformer  = new StringTransformer();            
            string transformedText = stringTransformer.Transform(text);
            
            Console.WriteLine(transformedText);            
            Console.ReadLine();
        }
    }
}