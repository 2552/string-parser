﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StringParser
{
    public class StringTransformer
    {
        public string Transform(string originalText)
        {
            // Todo: Контролировать длинну массива или использовать списки.
            char[] resultString = new char[] { };
            int j = 0;
            
            for (int i = 0; i < originalText.Length; i++)
            {

                if (originalText[i] == ']')
                {
                    string letters = "";
                    int number = 1;
                    string numberString = "";
                    string repeatedLetters = "";                    

                    for (j = i - 1; resultString[j] != '[' ; j--)
                    {
                        letters = resultString[j] + letters;
                    }
                    for (j = j - 1; j >= 0 && resultString[j] >= '0' && resultString[j] <= '9'; j--)
                    {
                        numberString = resultString[j] + numberString;
                    }
                    
                    // Todo: Обработать случай когда нет числа                    
                    number = int.Parse(numberString);

                    for (int k = 0; k < number; k++)
                    {
                        repeatedLetters += letters;
                    }
                    for (int l = 0; l < repeatedLetters.Length; l++)
                    {                  
                        // j должен идти со следующего номера после выхода из цикла
                        SetArrayElement(ref resultString,j,repeatedLetters[l]);
                        j++;
                    }
                }
                else
                {
                    SetArrayElement(ref resultString,j,originalText[i]);                    
                    j++;
                }
            }
            
            return new string (resultString);

        }
        
        private void SetArrayElement(ref char[] array, int index, char element )
        {
            while (index >= array.Length)
            {
                Array.Resize(ref array, (array.Length+1) * 2);                
            }
            
            array[index] = element;
        }
        
    }
}
