﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StringParser
{
    class Stack
    {
        private List<object> _stack;

        public void Push(object obj)
        {
            _stack.Add(obj);   
        }

        public object Pop()
        {
            object element;

            if (_stack.Count == 0)
            { 
                return null; 
            }
            
            element = _stack[_stack.Count - 1];
            
            return element;            
        }
    }
}
