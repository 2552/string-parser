﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using StringParser;

namespace StringParser.Test
{
    public class StringTransformerTests
    {
        [Fact]
        public void TransformStringReturnTransformedString()
        {
            //Подготовка
            string testString = "3[sd]";
            string expectedString = "sdsdsd";

            //Исполнение
            StringTransformer stringTransformer = new StringTransformer();
            string actualString = stringTransformer.Transform(testString);

            //Проверка
            Assert.Equal(expectedString, actualString);
        }
    }
}
